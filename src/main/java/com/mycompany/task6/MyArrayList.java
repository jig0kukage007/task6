package com.mycompany.task6;

public class MyArrayList<T> implements MyList<T> {

    @Override
    public void add(T elem) {
        return;
    }

    @Override
    public void insert(T elem, int index) {
        return;
    }

    @Override
    public void remove(int index) {
        return;
    }

    @Override
    public void remove(T value) {
        return;
    }

    @Override
    public int indexOf(T value) {
        return;
    }

    @Override
    public boolean contains(T value) {
        return;
    }

    @Override
    public boolean containsAll(MyList<T> values) {
        return;
    }

    @Override
    public T get(int index) {
        return;
    }

    @Override
    public void set(int index, T value) {
        return;
    }

    @Override
    public int size() {
        return;
    }

}
